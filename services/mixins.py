from autos.models import Announcement
from base.models import Publication
from choices.models import Kind, Condition, Color

ADDITIONAL_CONTEXT = {
    "index": {
        "popular_autos": Announcement.get_popular_announcements(),
        "latest_autos": Announcement.get_latest_announcements(),
    }
}


class ContextMixin:
    def get_extra_context(self, **kwargs):
        context = kwargs
        context["categories"] = Kind.objects.all()
        context["conditions"] = Condition.objects.all()
        context["colors"] = Color.objects.all()
        context["publications"] = Publication.get_3_latest_publications()
        context["popular_announcements"] = Announcement.get_popular_announcements()
        context["latest_announcements"] = Announcement.get_latest_announcements()
        return context
