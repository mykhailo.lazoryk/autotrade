import uuid

from django.core.handlers.wsgi import WSGIRequest


def client_directory_path(instance, filename) -> str:
    """
    Uploads client's photo to:
    media/clients_data/client_{id}/client_photo/{filename}
    """
    return f"clients_data/client_{instance.pk}/client_photo/{filename}"


def vehicle_directory_path(instance, filename) -> str:
    """
    Uploads vehicle's photo to:
    media/clients_data/client_{id}/client_announcements/announcement_{id}/{filename}
    """
    client_id = instance.vehicle.announcement.client.pk
    announcement_id = instance.vehicle.announcement.pk
    return f"clients_data/client_{client_id}/client_announcements/announcement_{announcement_id}/{filename}"


def publication_photo_directory_path(instance, filename) -> str:
    """
    Uploads publication's photo to:
    media/clients_data/client_{id}/client_publications/{slug}/{filename}
    """
    client_id = instance.client.pk
    publication_slug = instance.slug
    return f"clients_data/client_{client_id}/client_publications/{publication_slug}/{filename}"


def combine_context(con1: dict, con2: dict) -> dict:
    return dict(**con1, **con2)


def get_url_name_from_request(request: WSGIRequest) -> str:
    return request.resolver_match.url_name


def generate_uuid():
    return uuid.uuid4()
