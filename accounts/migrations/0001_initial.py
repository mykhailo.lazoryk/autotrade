# Generated by Django 3.2.9 on 2021-11-30 12:10

import accounts.managers
from django.db import migrations, models
import django.db.models.deletion
import services.functions
import services.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("auth", "0012_alter_user_first_name_max_length"),
        ("choices", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Client",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("password", models.CharField(max_length=128, verbose_name="password")),
                (
                    "last_login",
                    models.DateTimeField(
                        blank=True, null=True, verbose_name="last login"
                    ),
                ),
                (
                    "is_superuser",
                    models.BooleanField(
                        default=False,
                        help_text="Designates that this user has all permissions without explicitly assigning them.",
                        verbose_name="superuser status",
                    ),
                ),
                (
                    "email",
                    models.EmailField(
                        error_messages={
                            "unique": "Пользователь с таким емейлом уже существует!"
                        },
                        help_text="Обязательное поле! Введите емейл в формате: my_email@example.com",
                        max_length=254,
                        unique=True,
                        verbose_name="Емейл",
                    ),
                ),
                ("first_name", models.CharField(max_length=150, verbose_name="Имя")),
                ("last_name", models.CharField(max_length=150, verbose_name="Фамилия")),
                (
                    "birthday",
                    models.DateField(
                        blank=True, null=True, verbose_name="День Рождения"
                    ),
                ),
                (
                    "photo",
                    models.ImageField(
                        default="default_data/default_photo.png",
                        upload_to=services.functions.client_directory_path,
                        verbose_name="Фотография",
                    ),
                ),
                (
                    "telegram",
                    models.URLField(
                        blank=True,
                        help_text="Введите ссылку на свой телеграм в формате: https://t.me/username",
                        validators=[services.validators.telegram_link_validator],
                        verbose_name="Телеграм",
                    ),
                ),
                (
                    "uuid",
                    models.UUIDField(
                        db_index=True,
                        default=services.functions.generate_uuid,
                        unique=True,
                    ),
                ),
                (
                    "date_joined",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="Дата создания"
                    ),
                ),
                (
                    "is_staff",
                    models.BooleanField(
                        default=False,
                        help_text="Designates whether the user can log into this admin site.",
                        verbose_name="Персонал",
                    ),
                ),
                (
                    "is_active",
                    models.BooleanField(
                        default=True,
                        help_text="Designates whether this user should be treated as active. Unselect this instead of deleting accounts.",
                        verbose_name="Активирован",
                    ),
                ),
                (
                    "city",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="choices.city",
                        verbose_name="Город",
                    ),
                ),
                (
                    "groups",
                    models.ManyToManyField(
                        blank=True,
                        help_text="The groups this user belongs to. A user will get all permissions granted to each of their groups.",
                        related_name="user_set",
                        related_query_name="user",
                        to="auth.Group",
                        verbose_name="groups",
                    ),
                ),
                (
                    "user_permissions",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Specific permissions for this user.",
                        related_name="user_set",
                        related_query_name="user",
                        to="auth.Permission",
                        verbose_name="user permissions",
                    ),
                ),
            ],
            options={
                "verbose_name": "Клиент",
                "verbose_name_plural": "Клиенты",
            },
            managers=[
                ("objects", accounts.managers.ClientManager()),
            ],
        ),
    ]
