from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from accounts.models import Client


class ClientAdmin(UserAdmin):
    add_form_template = "admin/auth/user/add_form.html"
    change_user_password_template = None
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (
            "Личная информация",
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "birthday",
                    "city",
                    "photo",
                    "telegram",
                )
            },
        ),
        (
            "Доступы",
            {
                "fields": ("is_active", "is_staff", "is_superuser"),
            },
        ),
        ("Активность", {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2"),
            },
        ),
    )
    list_display = ("email", "first_name", "last_name", "is_staff")
    list_filter = ("is_staff", "is_superuser", "is_active")
    search_fields = ("first_name", "last_name", "email")
    ordering = ("email",)
    readonly_fields = ("date_joined",)


admin.site.register(Client, ClientAdmin)
