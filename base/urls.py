from django.conf.urls import url
from django.urls import path
from django.views.generic import RedirectView

from base.views import Index, Contact

urlpatterns = [
    path("", Index.as_view(), name="index"),
    path("contact/", Contact.as_view(), name="contact"),
    url(r"^favicon\.ico$", RedirectView.as_view(url="/static/image/favicon.ico")),
]
