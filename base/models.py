from django.db import models
from django.utils.translation import gettext_lazy as _

from accounts.models import Client
from autos.models import Announcement
from services.functions import publication_photo_directory_path


class Favorite(models.Model):
    client = models.ForeignKey(
        verbose_name=_("Клиент"),
        to=Client,
        on_delete=models.CASCADE,
        related_name="favorite_client",
    )
    announcement = models.ForeignKey(
        verbose_name=_("Объявление"),
        to=Announcement,
        on_delete=models.CASCADE,
        related_name="favorite_announcement",
    )

    objects = models.Manager()

    class Meta:
        verbose_name = _("Избранное клиентом объвление")
        verbose_name_plural = _("Избранные клиентом объявления")

    def __str__(self):
        return f"{self.client} to {self.announcement}"


class Publication(models.Model):
    client = models.ForeignKey(
        verbose_name=_("Автор публикации"),
        to=Client,
        on_delete=models.CASCADE,
        related_name="publication",
    )
    title = models.CharField(
        _("Заголовок"), max_length=50, blank=False, default="Заголовок"
    )
    slug = models.SlugField(
        verbose_name=_("URL"), max_length=50, db_index=True, unique=True
    )
    content = models.TextField(
        _("Содержание статьи"), blank=False, default="Содержание статьи"
    )
    image = models.ImageField(
        _("Фотография на главную страницу"),
        upload_to=publication_photo_directory_path,
        default="default_data/default_publication_photo.png",
    )
    show_author = models.BooleanField(_("Отображать автора"), default=True)
    is_active = models.BooleanField(_("Активна"), default=True)
    upload_datetime = models.DateTimeField(
        _("Время и дата создания"), auto_now_add=True, editable=False
    )

    objects = models.Manager()

    class Meta:
        verbose_name = _("Публикация на главную страницу")
        verbose_name_plural = _("Публикации на главную страницу")

    def __str__(self):
        return f"{self.title} by {self.client}"

    @staticmethod
    def get_3_latest_publications():
        return Publication.objects.all().order_by("-upload_datetime")[:3]
