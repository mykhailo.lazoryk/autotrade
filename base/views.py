from django.views.generic import TemplateView

from services.functions import combine_context
from services.mixins import ContextMixin


class Index(ContextMixin, TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        extra_context = self.get_extra_context()
        return combine_context(context, extra_context)


class Contact(ContextMixin, TemplateView):
    template_name = "contact.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        extra_context = self.get_extra_context()
        return combine_context(context, extra_context)
