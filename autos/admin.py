from django.contrib import admin  # noqa:

from autos.models import Announcement, Vehicle, VehiclePhoto

admin.site.register(Announcement)
admin.site.register(Vehicle)
admin.site.register(VehiclePhoto)
