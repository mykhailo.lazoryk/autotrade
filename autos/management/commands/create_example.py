from time import sleep

from django.core.management.base import BaseCommand
from django.db import IntegrityError

from accounts.models import Client
from autos.models import Announcement, Vehicle, VehiclePhoto
from choices.models import Kind, Brand, City, Color, Condition
from services.loggers import colorize


def create_or_get_test_client_instance():
    try:
        client = Client.objects.create(email="test_example@test.com")
        client.set_password("123test123")
        client.save()
    except IntegrityError:
        client = Client.objects.get(email="test_example@test.com")

    return client


def create_and_return_announcement(client):
    res = Announcement.objects.create(client=client, title="Тестовое объявление")
    return res


def create_and_return_vehicle(announcement):
    res = Vehicle.objects.create(
        announcement=announcement,
        kind=Kind.objects.all()[0],
        brand=Brand.objects.all()[0],
        model="Тестовая модель",
        year_of_production=2021,
        run=1000,
        city=City.objects.all()[0],
        price=10000,
        color=Color.objects.all()[0],
        condition=Condition.objects.all()[0],
        description="Тестовое описание",
    )
    return res


def create_vehicle_photo(vehicle):
    VehiclePhoto.objects.create(
        vehicle=vehicle, photo="default_data/default_auto_photo.jpg"
    )


class Command(BaseCommand):
    help = (
        "Creates example instance of 'Announcement <- Vehicle <- VehiclePhoto' models."
    )

    def add_arguments(self, parser):
        parser.add_argument("repeat", nargs="+", type=int)

    def handle(self, *args, **options):
        repeat_number = options["repeat"][0]
        client = create_or_get_test_client_instance()
        for counter in range(repeat_number):
            announcement = create_and_return_announcement(client)
            vehicle = create_and_return_vehicle(announcement)
            create_vehicle_photo(vehicle)
            print(colorize("Yellow", f"Created new example instance ({counter + 1})"))
            sleep(0.2)
        return print(colorize("Green", "Success!"))
