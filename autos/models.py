import os
from random import sample

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models, OperationalError
from django.utils.translation import gettext_lazy as _

from accounts.models import Client
from choices.models import City, Kind, Brand, Color, Condition
from services.constants import ACTUAL_YEAR
from services.functions import vehicle_directory_path, generate_uuid


class Announcement(models.Model):
    client = models.ForeignKey(
        verbose_name=_("Автор объявления"),
        to=Client,
        on_delete=models.CASCADE,
        related_name="announcement",
    )
    title = models.CharField(
        _("Заголовок"),
        max_length=100,
        blank=False,
    )
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    active = models.BooleanField(_("Активен"), default=True)
    created = models.DateTimeField(
        _("Время создания"), auto_now_add=True, editable=False
    )
    expiration_time_in_days = models.IntegerField(_("Время дезактивации"), default=30)

    objects = models.Manager()

    class Meta:
        verbose_name = _("Объявление о продаже")
        verbose_name_plural = _("Объявления о продаже")

    def __str__(self):
        return f"{self.client} - {self.title}"

    @classmethod
    def get_popular_announcements(cls):
        try:
            items = list(cls.objects.all())
            return sample(items, 2)
        except OperationalError:
            # TODO
            return False
        except ValueError:
            # TODO
            return False

    @classmethod
    def get_latest_announcements(cls):
        try:
            return cls.objects.all().order_by("-created")[:8]
        except OperationalError:
            # TODO
            return False


class Vehicle(models.Model):
    announcement = models.OneToOneField(
        verbose_name=_("Объявление"),
        to=Announcement,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name="vehicle",
    )
    kind = models.ForeignKey(
        verbose_name=_("Тип"), to=Kind, blank=False, on_delete=models.CASCADE
    )
    brand = models.ForeignKey(
        verbose_name=_("Бренд"), to=Brand, blank=False, on_delete=models.CASCADE
    )
    model = models.CharField(_("Модель"), max_length=100, blank=False)
    year_of_production = models.IntegerField(
        _("Год производства"),
        validators=[
            MaxValueValidator(ACTUAL_YEAR),
            MinValueValidator(ACTUAL_YEAR - 200),
        ],
        default=ACTUAL_YEAR,
        blank=False,
    )
    run = models.IntegerField(
        _("Пробег"),
        blank=False,
        default=10,
        validators=[MaxValueValidator(3000000), MinValueValidator(1)],
        help_text="min=1, max=3'000'000",
    )
    city = models.ForeignKey(
        verbose_name=_("Город"), to=City, blank=False, on_delete=models.CASCADE
    )
    price = models.IntegerField(
        _("Стоимость в $"),
        blank=False,
        default=1,
        validators=[MaxValueValidator(3000000), MinValueValidator(1)],
        help_text="min=1, max=3'000'000",
    )
    color = models.ForeignKey(
        verbose_name=_("Цвет"), to=Color, blank=False, on_delete=models.CASCADE
    )
    condition = models.ForeignKey(
        verbose_name=_("Состояние"), to=Condition, blank=False, on_delete=models.CASCADE
    )
    description = models.TextField(
        _("Описание"), blank=True, default="Описание автомобиля"
    )
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    upload_date = models.DateTimeField(
        _("Дата и время добавления"), auto_now_add=True, editable=False
    )

    objects = models.Manager()

    class Meta:
        verbose_name = _("Транспортное средство")
        verbose_name_plural = _("Транспортные средства")

    def __str__(self):
        return f"{self.brand} {self.model}"

    def get_vehicle_photo(self):
        photo = self.vehicle_photo.latest("upload_date")
        print(photo.photo.url)
        return photo.photo.url


class VehiclePhoto(models.Model):
    vehicle = models.ForeignKey(
        verbose_name=_("Транспорт"),
        to=Vehicle,
        on_delete=models.CASCADE,
        related_name="vehicle_photo",
    )
    photo = models.ImageField(
        _("Фотография транспорта"),
        upload_to=vehicle_directory_path,
        default="default_data/default_vehicle_photo.png",
        blank=False,
    )
    upload_date = models.DateTimeField(
        _("Дата и время добавления"), auto_now_add=True, editable=False
    )

    objects = models.Manager()

    class Meta:
        verbose_name = _("Фото Транспортного средства")
        verbose_name_plural = _("Фото Транспортных средств")

    def get_photo_filename(self):
        return os.path.basename(self.photo.name)

    def __str__(self):
        return f"{self.vehicle} - {self.get_photo_filename()}"
