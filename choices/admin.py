from django.contrib import admin

from choices.models import City, Kind, Brand, Color, Condition

admin.site.register([City, Kind, Brand, Color, Condition])
